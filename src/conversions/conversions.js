import { Point, Orientation, Layout } from './structures';
import { config } from '../config';

const orientation = Orientation(
  Math.sqrt(3.0),
  Math.sqrt(3.0) / 2.0,
  0.0,
  3.0 / 2.0,
  Math.sqrt(3.0) / 3.0,
  -1.0 / 3.0,
  0.0,
  2.0 / 3.0,
  0.5
);
const layout = Layout(orientation, config.size, config.origin);

export function hexToPoint(cube) {
  const { size, origin } = layout;
  const x = (orientation.f0 * cube.z + orientation.f1 * cube.y) * size.x;
  const y = (orientation.f2 * cube.z + orientation.f3 * cube.y) * size.y;
  return Point(x + origin.x, y + origin.y);
}
