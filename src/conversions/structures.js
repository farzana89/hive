export function Point(x, y) {
  return { x, y };
}

export function Cube(x, y, z) {
  return { x, y, z };
}

export function Orientation(f0, f1, f2, f3, b0, b1, b2, b3, startAngle) {
  return {
    f0,
    f1,
    f2,
    f3,
    b0,
    b1,
    b2,
    b3,
    startAngle
  };
}

export function Layout(orientation, size, origin) {
  return { orientation, size, origin };
}
