import { hexToPoint } from '../conversions/conversions';
import { calculateNeighbours, areEqual } from './cubeCalculator';
import { setupPlayer, playTile } from './player';
import { config } from '../config';
import { PlayerTypes } from '../constants';
import { moveIsValid } from './moveValidation';

const { startingCube } = config;

function setupPlayers() {
  const player1 = setupPlayer(PlayerTypes.LIGHT);
  const player2 = setupPlayer(PlayerTypes.DARK);
  return [player1, player2];
}

function cubeExistsInList(cubeList, cube) {
  return cubeList.find(lst => areEqual(lst, cube));
}

function appendNeighbours(currNeighbours, cell, index, srcCells) {
  const srcCubes = srcCells.map(src => src.cube);
  return [
    ...currNeighbours,
    ...calculateNeighbours(cell.cube).filter(
      neighbour =>
        !cubeExistsInList(currNeighbours, neighbour) &&
        !cubeExistsInList(srcCubes, neighbour)
    )
  ];
}

function findNeighbours(cells) {
  return cells
    .filter(c => c.content !== '')
    .reduce(appendNeighbours, [])
    .map(cell => ({
      cube: cell,
      point: hexToPoint(cell),
      content: ''
    }));
}

function getGridCells() {
  const hexCells = [
    {
      cube: startingCube,
      point: hexToPoint(startingCube),
      content: ''
    }
  ];
  const neighbours = findNeighbours(hexCells);

  return neighbours ? [...hexCells, ...neighbours] : hexCells;
}

class Engine {
  constructor() {
    this.gridCells = getGridCells();
    this.endGame = false;
    this.players = setupPlayers();
  }

  updateGridCells() {
    const cellsContainingTiles = this.gridCells.filter(
      cell => cell.content !== ''
    );
    const neighbours = findNeighbours(this.gridCells);

    this.gridCells = neighbours
      ? [...cellsContainingTiles, ...neighbours]
      : this.gridCells;
  }

  updateCellContent(cube, value) {
    const cellToUpdate = this.gridCells.find(
      cell =>
        cell.cube.x === cube.x &&
        cell.cube.y === cube.y &&
        cell.cube.z === cube.z
    );

    if (cellToUpdate) {
      cellToUpdate.content = value;
    }
  }

  updatePlayerTiles(tile) {
    const player = this.players.find(p => p.colour === tile.colour);
    const updatedPlayer = playTile(player, tile);

    // TODO: Revisit this once basic turns in place
    return [updatedPlayer, this.players.find(p => p.colour !== player.colour)];
  }

  moveTile(srcCube, destCube, tile) {
    let validMove = true;

    // TODO: difference between playing and moving tiles needs to be clearer
    if (srcCube) {
      validMove = moveIsValid(this.gridCells, tile, srcCube, destCube);
    }
    if (validMove) {
      if (srcCube) {
        this.updateCellContent(srcCube, '');
      }
      this.updateCellContent(destCube, tile);
      this.players = this.updatePlayerTiles(tile);
      this.updateGridCells();
    } else {
      // console.log('invalid move');
    }
  }
}

export default Engine;
