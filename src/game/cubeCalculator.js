import { Cube } from '../conversions/structures';

const cubeDirections = [
  Cube(+1, -1, 0),
  Cube(+1, 0, -1),
  Cube(0, +1, -1),
  Cube(-1, +1, 0),
  Cube(-1, 0, +1),
  Cube(0, -1, +1)
];

export function calculateNeighbours(cube) {
  return cubeDirections.map(direction =>
    Cube(cube.x + direction.x, cube.y + direction.y, cube.z + direction.z)
  );
}

export function areEqual(cubeA, cubeB) {
  return cubeA.x === cubeB.x && cubeA.y === cubeB.y && cubeA.z === cubeB.z;
}
