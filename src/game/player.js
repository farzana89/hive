import { config } from '../config';
import { createTile } from './tiles/tileFactory';

const { tiles } = config;

function setupPlayerTiles(colour) {
  return tiles.map(tile => createTile(tile, colour));
}

export function setupPlayer(colour) {
  const availTiles = setupPlayerTiles(colour);

  return {
    colour,
    availTiles
  };
}

export function playTile(player, movedTile) {
  const { colour, availTiles } = player;
  const tile = availTiles.find(t => t.type === movedTile.type);
  if (tile) {
    const index = availTiles.indexOf(tile);
    availTiles.splice(index, 1);
  }
  return {
    colour,
    availTiles
  };
}
