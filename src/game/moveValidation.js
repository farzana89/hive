import { areEqual } from './cubeCalculator';

function moveIsValidForTile(tile, src, dest) {
  return (
    tile.getAvailMoves(src).filter(cube => areEqual(cube, dest)).length > 0
  );
}

function cellIsUnOccupied(gridCells, dest) {
  const gridCell = gridCells.find(cell => areEqual(cell.cube, dest));
  if (gridCell) {
    return gridCell.content === '';
  }
  return false;
}

export function moveIsValid(gridCells, tile, src, dest) {
  return tile.canStack
    ? moveIsValidForTile(tile, src, dest)
    : cellIsUnOccupied(gridCells, dest);
}
