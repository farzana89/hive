import { calculateNeighbours } from '../cubeCalculator';

class Beetle {
  constructor(colour) {
    this.type = 'beetle';
    this.colour = colour;
    this.canStack = true;
  }

  // eslint-disable-next-line class-methods-use-this
  getAvailMoves(location) {
    return calculateNeighbours(location);
  }
}

export default Beetle;
