import Queen from './Queen';
import Beetle from './Beetle';

export function createTile(type, colour) {
  if (type === 'queen') {
    return new Queen(colour);
  }
  if (type === 'beetle') {
    return new Beetle(colour);
  }
  return null;
}
