import { calculateNeighbours } from '../cubeCalculator';

class Queen {
  constructor(colour) {
    this.type = 'queen';
    this.colour = colour;
    this.canStack = false;
  }

  // eslint-disable-next-line class-methods-use-this
  getAvailMoves(location) {
    return calculateNeighbours(location);
  }
}

export default Queen;
