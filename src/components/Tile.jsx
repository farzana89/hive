import React from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import QueenIcon from '../images/QueenIcon';
import BeetleIcon from '../images/BeetleIcon';

const tileSource = {
  beginDrag(props) {
    return {
      text: props.type
    };
  },
  endDrag(props, monitor) {
    if (monitor.didDrop()) {
      const result = monitor.getDropResult();
      props.onMove(result);
    }
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

function renderTileIcon(type) {
  if (type === 'queen') {
    return <QueenIcon />;
  }
  if (type === 'beetle') {
    return <BeetleIcon />;
  }

  return '';
}

class Tile extends React.Component {
  render() {
    const { type, colour, isDragging, connectDragSource } = this.props;

    return connectDragSource(
      <div
        key={type}
        className={`tile ${colour} tile-${colour}`}
        style={{
          opacity: isDragging ? 0.5 : 1,
          cursor: 'move'
        }}
      >
        {renderTileIcon(type)}
      </div>
    );
  }
}

Tile.propTypes = {
  type: PropTypes.string.isRequired,
  colour: PropTypes.string.isRequired,
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired
};

export default DragSource('Tile', tileSource, collect)(Tile);
