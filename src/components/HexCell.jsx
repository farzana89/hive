import React from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';

const cellTarget = {
  drop(props) {
    return props.cube;
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

function style(point) {
  return {
    left: point.x,
    top: point.y
  };
}

class HexCell extends React.Component {
  render() {
    const { position, cube, children, connectDropTarget, isOver } = this.props;
    return connectDropTarget(
      <div key={cube} className={`hexagon ${isOver}`} style={style(position)}>
        {children === null ? cube : children}
      </div>
    );
  }
}

HexCell.propTypes = {
  position: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number
  }).isRequired,
  cube: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number,
    z: PropTypes.number
  }).isRequired,
  children: PropTypes.node.isRequired,
  isOver: PropTypes.bool.isRequired,
  connectDropTarget: PropTypes.func.isRequired
};

export default DropTarget('Tile', cellTarget, collect)(HexCell);
