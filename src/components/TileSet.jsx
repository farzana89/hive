import React from 'react';
import { GameContext } from '../Provider';
import Tile from './Tile';
import { PlayerTypes } from '../constants';

function renderTiles(colour, availTiles, moveTile) {
  return (
    <div
      key={`player-${colour}`}
      className="tile-set"
      style={{ float: `${colour === PlayerTypes.LIGHT ? 'left' : 'right'}` }}
    >
      {availTiles.map((tile, index) => (
        <Tile
          // eslint-disable-next-line react/no-array-index-key
          key={`${colour}-${index}`}
          type={tile.type}
          colour={tile.colour}
          onMove={destCube => {
            moveTile(null, destCube, tile);
          }}
        />
      ))}
    </div>
  );
}

function TileSet() {
  return (
    <GameContext.Consumer>
      {context => (
        <main className="tiles">
          {context.state.players.map(player =>
            renderTiles(player.colour, player.availTiles, context.moveTile)
          )}
        </main>
      )}
    </GameContext.Consumer>
  );
}

export default TileSet;
