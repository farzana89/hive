import React from 'react';
import { GameContext } from '../Provider';
import HexCell from './HexCell';
import Tile from './Tile';

function Grid() {
  return (
    <GameContext.Consumer>
      {context => (
        <main className="hexagon-container">
          {context.state.gridCells.map(cell => (
            <HexCell
              key={`${cell.point.x}+${cell.point.y}`}
              position={cell.point}
              cube={cell.cube}
            >
              {cell.content ? (
                <Tile
                  type={cell.content.type}
                  colour={cell.content.colour}
                  onMove={destCube => {
                    context.moveTile(cell.cube, destCube, cell.content);
                  }}
                />
              ) : (
                ''
              )}
            </HexCell>
          ))}
        </main>
      )}
    </GameContext.Consumer>
  );
}

export default Grid;
