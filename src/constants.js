export const PlayerTypes = Object.freeze({
  LIGHT: 'light',
  DARK: 'dark'
});
