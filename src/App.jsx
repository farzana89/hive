import React from 'react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { GameProvider } from './Provider';
import Grid from './components/Grid';
import TileSet from './components/TileSet';

// DragDropContext expects a class
// eslint-disable-next-line react/prefer-stateless-function
class App extends React.Component {
  render() {
    return (
      <GameProvider>
        <TileSet />
        <Grid />
      </GameProvider>
    );
  }
}

export default DragDropContext(HTML5Backend)(App);
