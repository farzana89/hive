import React from 'react';
import PropTypes from 'prop-types';
import Engine from './game/Engine';

const GameContext = React.createContext();

class GameProvider extends React.Component {
  constructor() {
    super();
    this.engine = new Engine();
    this.state = {
      gridCells: this.engine.gridCells,
      players: this.engine.players
    };
  }

  updateState() {
    this.setState({
      gridCells: this.engine.gridCells,
      players: this.engine.players
    });
  }

  render() {
    const { children } = this.props;
    return (
      <GameContext.Provider
        value={{
          state: this.state,
          moveTile: (...args) => {
            this.engine.moveTile(...args);
            this.updateState();
          }
        }}
      >
        {children}
      </GameContext.Provider>
    );
  }
}

GameProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export { GameProvider, GameContext };
