import Queen from '../../game/tiles/Queen';
import Beetle from '../../game/tiles/Beetle';
import { moveIsValid } from '../../game/moveValidation';
import { Cube } from '../../conversions/structures';
import { PlayerTypes } from '../../constants';

const queenTile = new Queen(PlayerTypes.LIGHT);
const beetleTile = new Beetle(PlayerTypes.DARK);

const gridCells = [
  {
    cube: Cube(0, 0, 0),
    content: queenTile
  },
  { cube: Cube(1, -1, 0), content: beetleTile },
  { cube: Cube(1, 0, -1), content: '' },
  { cube: Cube(0, 1, -1), content: '' },
  { cube: Cube(-1, 1, 0), content: '' },
  { cube: Cube(-1, 0, 1), content: '' },
  { cube: Cube(0, -1, 1), content: '' }
];

describe('Move validation', () => {
  it('should retrun true move is valid', () => {
    const result = moveIsValid(
      gridCells,
      beetleTile,
      Cube(0, 0, 0),
      Cube(1, 0, -1)
    );
    expect(result).toBe(true);
  });

  it('should retrun false move is not valid', () => {
    const result = moveIsValid(
      gridCells,
      queenTile,
      Cube(0, 0, 0),
      Cube(1, 0, -2)
    );
    expect(result).toBe(false);
  });

  it('should retrun true move is valid due to dest being occupied', () => {
    const result = moveIsValid(
      gridCells,
      queenTile,
      Cube(0, 0, 0),
      Cube(1, -1, 0)
    );
    expect(result).toBe(false);
  });
});
