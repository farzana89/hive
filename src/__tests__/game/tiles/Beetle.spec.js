import Beetle from '../../../game/tiles/Beetle';
import { Cube } from '../../../conversions/structures';
import { PlayerTypes } from '../../../constants';

describe('Beetle', () => {
  it('should setup new beetle tile', () => {
    const cube = Cube(0, 0, 0);
    const tile = new Beetle(PlayerTypes.LIGHT);

    expect(tile.canStack).toBe(true);
    expect(tile.type).toBe('beetle');
    expect(tile.getAvailMoves(cube)).toEqual([
      Cube(1, -1, 0),
      Cube(1, 0, -1),
      Cube(0, 1, -1),
      Cube(-1, 1, 0),
      Cube(-1, 0, 1),
      Cube(0, -1, 1)
    ]);
  });
});
