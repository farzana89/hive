import Queen from '../../../game/tiles/Queen';
import { Cube } from '../../../conversions/structures';
import { PlayerTypes } from '../../../constants';

describe('Queen', () => {
  it('should setup new queen tile', () => {
    const cube = Cube(0, 0, 0);
    const tile = new Queen(PlayerTypes.LIGHT);

    expect(tile.canStack).toBe(false);
    expect(tile.type).toBe('queen');
    expect(tile.getAvailMoves(cube)).toEqual([
      Cube(1, -1, 0),
      Cube(1, 0, -1),
      Cube(0, 1, -1),
      Cube(-1, 1, 0),
      Cube(-1, 0, 1),
      Cube(0, -1, 1)
    ]);
  });
});
