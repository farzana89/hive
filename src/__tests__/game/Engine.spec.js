import Engine from '../../game/Engine';
import { Cube } from '../../conversions/structures';
import { PlayerTypes } from '../../constants';

let engine = null;

beforeEach(() => {
  engine = new Engine();
});

function findCell(cells, cube) {
  return cells.find(
    cell =>
      cell.cube.x === cube.x && cell.cube.y === cube.y && cell.cube.z === cube.z
  );
}

const queenTile = {
  colour: PlayerTypes.LIGHT,
  type: 'queen'
};

describe('Game engine', () => {
  it('Get starting grid cells', () => {
    const result = engine.gridCells;

    expect(result.length).toBe(1);
  });

  it('Should clear content of source cell and populate dest cell when moving tile', () => {
    const dest = Cube(1, -1, 0);
    engine.moveTile(null, Cube(0, 0, 0), queenTile);

    expect(findCell(engine.gridCells, dest).content).toEqual('');

    engine.moveTile(null, dest, queenTile);

    expect(findCell(engine.gridCells, dest).content).toEqual(queenTile);
  });

  it('Should clear neighbours of empty cells when tile moves', () => {
    const src = Cube(0, 0, 0);
    const dest = Cube(1, -1, 0);

    engine.moveTile(null, Cube(0, 0, 0), queenTile);

    let emptyCells = engine.gridCells.filter(x => x.content === '');
    expect(emptyCells.length).toBe(6);

    engine.moveTile(src, dest, queenTile);

    emptyCells = engine.gridCells.filter(x => x.content === '');
    expect(emptyCells.length).toBe(6);
  });

  it('Should update grid cells when a tile is moved into the grid', () => {
    const src = null;
    const dest = Cube(0, 0, 0);

    engine.moveTile(src, dest, queenTile);
    expect(findCell(engine.gridCells, dest).content).toEqual(queenTile);
  });
});
