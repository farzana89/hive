import { setupPlayer, playTile } from '../../game/player';
import { PlayerTypes } from '../../constants';

describe('Player', () => {
  it('should setup new player', () => {
    const player = setupPlayer(PlayerTypes.LIGHT);

    expect(player.colour).toBe('light');
    expect(player.availTiles.length).toBe(3);
  });

  it('return updated player tiles when a tile is placed', () => {
    const player = setupPlayer(PlayerTypes.DARK);

    const updatedPlayer = playTile(player, {
      type: 'queen',
      colour: 'dark'
    });

    expect(updatedPlayer.availTiles).not.toContain({
      type: 'queen',
      colour: 'dark'
    });
  });
});
