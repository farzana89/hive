import { Cube } from '../../conversions/structures';
import { calculateNeighbours } from '../../game/cubeCalculator';

describe('Cube', () => {
  it('Calculate neighbours for cube', () => {
    const cube = Cube(0, 0, 0);

    const result = calculateNeighbours(cube);

    expect(result).toEqual([
      Cube(+1, -1, 0),
      Cube(+1, 0, -1),
      Cube(0, +1, -1),
      Cube(-1, +1, 0),
      Cube(-1, 0, +1),
      Cube(0, -1, +1)
    ]);
  });
});
