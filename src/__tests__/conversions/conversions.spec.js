import { Cube, Point } from '../../conversions/structures';
import { hexToPoint } from '../../conversions/conversions';
import { config } from '../../config';

const { origin } = config;

describe('Converting cube coords to point coords', () => {
  it('Center cube converted from hex to point should equal origin', () => {
    const cube = Cube(0, 0, 0);

    const result = hexToPoint(cube);

    expect(result).toEqual(Point(origin.x, origin.y));
  });

  it('Convert cube from hex to point', () => {
    const cube = Cube(0, +1, -1);

    const result = hexToPoint(cube);

    expect(result).toEqual(Point(656.6987298107781, 475));
  });
});
