# HIVE

[![Node: 10.2.1](https://img.shields.io/badge/node-10.2.1-blue.svg)]()
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

A basic hive game.

## Getting started

Run:
`npm start`

Test:
`npm test`
